-- Copyright 2008 Steven Barth <steven@midlink.org>
-- Copyright 2008 Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

-- local dbg = require "luci.dbg"
-- local dlog = dbg.Log:new()
local fs = require "nixio.fs"

m = Map("network", translate("Wi-SUN"))
m.pageaction = false
m:section(SimpleSection).template = "admin_network/wisun_overview"


return m
