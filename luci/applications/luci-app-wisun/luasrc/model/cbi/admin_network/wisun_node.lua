-- Copyright 2008 Steven Barth <steven@midlink.org>
-- Copyright 2008-2011 Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

local dbg = require "luci.dbg"
local dlog = dbg.Log:new()
local tostring = tostring

arg[1] = arg[1] or ""

-- dlog:dbg("wisun node test " .. arg[1])

m = Map("network", translate("Wi-SUN") .. " - " .. arg[1]:upper())
m.pageaction = false
m:section(SimpleSection).template = "admin_network/wisun_node"

return m
