
local io = require "io"
local debug = require "debug"
local os = require "os"
local string = require "string"
local tostring, pairs = tostring, pairs
local print, type, setmetatable = print, type, setmetatable
local next, rawget = next, rawget

module("luci.dbg")

dbg_level = {
    OFF = 1,
    ERROR = 2,
    DEBUG = 3,
    WARNING = 4,
    INFO = 5
}

Log = {
    _dbg_file = "/tmp/luci_dbg.log",
    _dbg_level = 4,
    _is_open = 0
}


function Log:new ()
    return Log
end


function Log:open(fname)
    if fname then
       self._dbg_file = fname 
    end

    if not self._dbg_handle then
        self._dbg_handle = io.open(self._dbg_file, "a+")
    end

    if self._dbg_handle then
        self._is_open = 1 
    end
    
    return self._dbg_handle
end


function Log:close()
    if self._dbg_handle then
        self._dbg_handle:close()
        self._dbg_handle = nil
        self._is_open = 0 
    end
end


function Log:trans_table(t)

    local r_str
    for k, v in pairs(t) do
        Log:dbg(k .. ">>")    
        if type(v) == "table" then
            for ik, iv in pairs(v) do
                if type(ik) == "string" then
                    r_str = ik
                else
                    r_str = "no string"
                end
                if (type(iv) == "string") or (type(iv) == "number") then
                    Log:dbg(r_str .. "-- value: " .. iv)
                else
                    Log:dbg(r_str .. "-- type: " .. type(iv))
                end
            end
        end
    end

    return r_str or ""
end


function Log:travel_table(t)

    -- if type(t) != "table" then
    --     return
    -- end

    Log:dbg("------------ table travell ------------")
    for k,v in pairs(t) do
        if type(v) == "table" then
            Log:dbg("Key=" .. k .. " and Value = table")
            if not string.find(k, "_M") then
                Log:travel_table(v)
            end
        else 
            if (type(v) == "string") or (type(v) == "number") then
                Log:dbg("Key=" .. k .. " and Value=" .. v)
            else
                Log:dbg("Key=" .. k .. " and Value type=" .. type(v))
            end
       end
    end
    Log:dbg("---------------------------------------")
end


function Log:write(d_level, log_str)

    if d_level > self._dbg_level then
        return
    end

    if self._is_open == 0 then
        self:open()
    end

    local info = debug.getinfo(3)
    -- info format
    -- type(what) = string
    -- type(func) = function
    -- type(lastlinedefined) = number
    -- type(source) = string [@file name]
    -- type(currentline) = number [__LINE__]
    -- type(namewhat) = string
    -- type(linedefined) = number
    -- type(short_src) = string [__FILE__]
    -- type(name) = string [__func__]

    local _cur_t = "[" .. os.date("%H:%M:%S") .. "] "
    local _cur_info = info.short_src .. " > "

    -- if info.what == "main" then
    --     _cur_info = _cur_info .. info.what .. ' :'
    -- else
    --     _cur_info = _cur_info .. info.name .. ' :'
    -- end

    if info.what then
        _cur_info = _cur_info .. info.what .. ' :'
    end

    if info.name then
        _cur_info = _cur_info .. info.name .. ' :'
    end

    _cur_info = _cur_info .. info.currentline .. ' >> '

    -- if type(log_str) == "table" then
    --     _cur_info = _cur_info .. self:trans_table(log_str)
    -- end

    if self._dbg_handle then
       self._dbg_handle:write(_cur_t .. _cur_info .. log_str .. "\n\r") 
       self._dbg_handle:flush()
    end
end


function Log:info(log_str)
    self:write(dbg_level["INFO"], "=INFO= " .. log_str)
end


function Log:warn(log_str)
    self:write(dbg_level["WARNING"], "=WARN= " .. log_str) 
end


function Log:dbg(log_str)
    self:write(dbg_level["DEBUG"], "=DEBUG= " .. log_str)
end


function Log:err(log_str)
    self:write(dbg_level["ERROR"], "=ERROR= " .. log_str)
end


local dbg_file = "/tmp/luci_dbg.log"

function log(log_str)
    local info = debug.getinfo(2)
    -- info format
    -- type(what) = string
    -- type(func) = function
    -- type(lastlinedefined) = number
    -- type(source) = string [@file name]
    -- type(currentline) = number [__LINE__]
    -- type(namewhat) = string
    -- type(linedefined) = number
    -- type(short_src) = string [__FILE__]
    -- type(name) = string [__func__]

    local _cur_t = "[" .. os.date("%H:%M:%S") .. "] "
    local _fd = io.open(dbg_file, "w+")
    local _cur_info = info.short_src .. " > "

    if info.what == "main" then
        _cur_info = _cur_info .. info.what .. ' :'
    else
        _cur_info = _cur_info .. info.name .. ' :'
    end
    _cur_info = _cur_info .. info.currentline .. ' >> '
    print(_cur_t .. _cur_info .. log_str)

    if _fd then
        _fd:write(_cur_t .. _cur_info .. log_str)    
        _fd:close()
    end
end


function test()
    a = Log:new()
    a:open()
    a:write("this is my test")
    a:close()
end
