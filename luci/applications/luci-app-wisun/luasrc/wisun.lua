

local dbg = require "luci.dbg"
local dlog = dbg.Log:new()

local string = require("string")
local json = require("luci.json")
local tonumber, tostring = tonumber, tostring

local luci  = {}
luci.util   = require "luci.util"
luci.sys    = require "luci.sys"

module("luci.wisun")

wisun_node = {}

function wisun_node.root_cmd(parm)
    local method = parm['method'] or "get"
    local ip = parm['ip'] or "2002::1122:3344:5566:1"
    local api = parm['api'] or "/vcSys/info"
    local res = luci.sys.exec("coap-client -m " .. method .. " -b 10 coap://[" .. ip .. "]:5683" .. api)

    -- dlog:dbg(res)
    return json.decode(string.gsub(res, "v:1 t:CON c:GET i:%x+ {} %[%s%]", ""))
end

function wisun_node.list()
    local data = {}
    local info = wisun_node.root_cmd({api = "/vcRpl/joinedTableNum"})
    local link_num = tonumber(info["joined table num"])
    local join_info
    local link_info
     
    if link_num > 0 then
        join_info = wisun_node.root_cmd({api = "/vcRpl/joinedTable"}) 
        link_info = wisun_node.root_cmd({api = "/vcRpl/linkTable"})

        for l = 1, link_num do
            data[l] = {
                ['INDEX'] = tostring(l),
                ['MAC Address'] = string.upper(join_info["joined table"][l]["mac"]),
                ['IP'] = join_info["joined table"][l]["ip"],
                ['Etx'] = tostring(link_info["link table"][l]["etx"]),
                ['RSSI'] = tostring(link_info["link table"][l]["rssi"])
            }
        end
    end

    return data
end

