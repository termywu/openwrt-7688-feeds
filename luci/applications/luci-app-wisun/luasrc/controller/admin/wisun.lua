
local dbg = require "luci.dbg"
local dlog = dbg.Log:new()
local wisun = require "luci.wisun"
local json = require("luci.json")

local driver = require('luasql.sqlite3')
local env = driver.sqlite3()
local db = env:connect('/Media/SD-P1/dr_storage.db', 3000)
local table = require('table')
local tostring = tostring

module("luci.controller.admin.wisun", package.seeall)

function index()

	local page
    local uci = luci.model.uci.cursor()

	page = node("admin", "wisun")
	page.target = firstchild()
	page.title  = _("WiSUN")
	page.order  = 50
	page.index  = true

    page = entry({"admin", "wisun", "root"}, template("admin_wisun/root"), _("Root Information"), 10)
    page.leaf = true

    if uci:get('factory', 'oem', 'name') == 'DR_STORAGE' then
        page = entry({"admin", "wisun", "node"}, call("render_node"), _("Node Table List"), 20)
        page.leaf = true
    else
        page = entry({"admin", "wisun", "node"}, template("admin_wisun/node"), _("Node"), 20)
        page.leaf = true
    end

    page = entry({"admin", "wisun", "node_table"}, call("wisun_node_table"), nil)
    page.leaf = true

    page = entry({"admin", "wisun", "wisun_ping"}, call("wisun_ping"), nil)
    page.leaf = true

    if uci:get('factory', 'oem', 'name') == 'DR_STORAGE' then
        page = entry({"admin", "wisun", "drstorage"}, call("dr_storage_data"), nil)
        page.leaf = true

        page = entry({"admin", "wisun", "last"}, call("output_last"), nil)
        page.leaf = true

        page = entry({"admin", "wisun", "get_data"}, call("db_get_data"), nil)
        page.leaf = true

        page = entry({"admin", "wisun", "update_date"}, call("db_update_date"), nil)
        page.leaf = true

        page = entry({"admin", "wisun", "update_node_mac"}, call("db_update_mac"), nil)
        page.leaf = true

        page = entry({"admin", "wisun", "database"}, call("render_database"), _("Data Center"), 30)
        page.leaf = true
    end
end

function wisun_node_table()
    local data = wisun.wisun_node.list()
    luci.http.write(json.encode(data))
end

function wisun_ping(target_ip)
    local ip = "coap://[" .. target_ip .. "]:5683/vcShell/shellHandler"
    luci.sys.exec('coap-client -m post -e "ping6 1 c 64" ' .. ip)
end

function render_node()
    local ndata = {}
    if db == nil then
        luci.template.render("admin_wisun/node", {ndata=ndata})
        return
    end

    local ans = db:execute[[
        SELECT created_at, humidity FROM DR_TEMPHUMI
    ]]

    local created_at, humidity = ans:fetch()
    while created_at do
        table.insert(ndata, created_at)
        table.insert(ndata, humidity)
        created_at, humidity = ans:fetch()
    end

    luci.template.render("admin_wisun/node", {ndata=ndata})
    ans:close()
end

function render_database()
--    local ans = db:execute[[
--        SELECT created_at, humidity FROM DR_TEMPHUMI ORDER BY id DESC LIMIT 200
--    ]]

    local ndata = {}

    -- local ans = db:execute[[
    -- SELECT mac_addr FROM DR_TEMPHUMI GROUP BY mac_addr
    -- ]]
    --
    -- mac_addr = ans:fetch()
    -- local mac_list = {}
    -- while mac_addr do
    --     table.insert(mac_list, mac_addr)
    --     mac_addr = ans:fetch()
    -- end
    -- ndata['mac_list'] = mac_list
    -- ans:close()

    -- ans = db:execute[[
    -- SELECT date(created_at) FROM DR_TEMPHUMI GROUP BY date(created_at)
    -- ]]
    --
    -- created_at = ans:fetch()
    --
    -- local render_date = created_at
    --
    -- local date_list = {}
    -- while created_at do
    --     table.insert(date_list, created_at)
    --     created_at = ans:fetch()
    -- end
    -- ndata['date_list'] = date_list
    -- ans:close()


--[[
    if render_date then
        qstr = "SELECT created_at, mac_addr, temperature, humidity FROM DR_TEMPHUMI WHERE date(created_at) = date('" .. render_date .. "')"
        ans = db:execute(qstr)

        local created_at, mac_addr, temperature, humidity = ans:fetch()
        while created_at do
            local record = {}
            record['created_at'] = created_at
            record['mac_addr'] = mac_addr
            record['temperature'] = temperature
            record['humidity'] = humidity

            table.insert(ndata, record)
            created_at, mac_addr, temperature, humidity = ans:fetch()
        end
    end
--]]
    luci.template.render("admin_wisun/data-center", {ndata=ndata})
    -- ans:close()
end

--[[ 
quert syntax
http://%ip%/admin/wisun/dr_storage/%quert_string%
quert string list:
select=3&s0=[column1]&s1=[column2]&s2=[column3]
filter=[column]&logic=[G,S,E,NG,NS,NE]
--]]

function dr_storage_data(query_str)
    local t = {}
    for k, v in string.gmatch(query_str, "(%w+)=(%w+)") do
        t[k] = v
    end

    if t["range"] ~= nil then
        dlog:dbg("got range")
    end

    if t["name"] ~= nil then
        dlog:dbg("got name")
    end
    
    if t["select"] ~= nil then
        dlog:dbg("got select")
    end
end

function output_last()
    local ndata = {}
    local index = 1

    local ans = db:execute[[
    SELECT created_at, mac_addr, temperature, humidity FROM DR_TEMPHUMI WHERE created_at > datetime('now', '-3 minutes')
        ]]

    local created_at, mac_addr, temperature, humidity = ans:fetch()
    while created_at do
        ndata[index] = {
            ['date'] = tostring(created_at),
            ['macaddr'] = tostring(mac_addr),
            ['humidity'] = tostring(humidity),
            ['temperature'] = tostring(temperature)
        }
        created_at, mac_addr, temperature, humidity = ans:fetch()
        index = index + 1
    end
    ans:close()
    luci.http.write(json.encode(ndata))
end

function db_update_date(query_str)
    local t = {}

    if db == nil then
        return
    end

    for k, v in string.gmatch(query_str, "(%w+)=(%d+%.%d+%.%d+%.%d+)") do
        t[k] = v
    end

    for k, v in string.gmatch(query_str, "(%w+)=(%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x%x)") do
        t[k] = v
    end

    for k, v in string.gmatch(query_str, "(%w+)=(%d+%-%d+%-%d+)") do
        t[k] = v
    end

    local qstr = "SELECT created_at, mac_addr, temperature, humidity FROM DR_TEMPHUMI \
                  WHERE date(created_at) = date('" .. t['date'] .. "') AND mac_addr='" .. string.lower(t['node']) .. "'"
    local ans = db:execute(qstr)

    local ndata = {}
    local index = 1

    local created_at, mac_addr, temperature, humidity = ans:fetch()
    while created_at do
        ndata[index] = {
            ['date'] = tostring(created_at),
            ['macaddr'] = tostring(mac_addr),
            ['humidity'] = tostring(humidity),
            ['temperature'] = tostring(temperature)
        }
        created_at, mac_addr, temperature, humidity = ans:fetch()
        index = index + 1
    end
    ans:close()
    luci.http.write(json.encode(ndata))
end

function db_update_mac(spec_mac)
    if db == nil then
        return
    end
    local qstr = "SELECT date(created_at) FROM DR_TEMPHUMI \
                  WHERE mac_addr='" .. string.lower(spec_mac) .. "' GROUP BY date(created_at)"
    local ans = db:execute(qstr)

    local ndata = {}
    local index = 1

    local created_at = ans:fetch()
    while created_at do
        ndata[index] = tostring(created_at)
        created_at = ans:fetch()
        index = index + 1
    end
    ans:close()
    luci.http.write(json.encode(ndata))
end

function db_get_data()

    if db == nil then
        return
    end

    local ndata = {}
    local ans = db:execute[[
    SELECT mac_addr FROM DR_TEMPHUMI GROUP BY mac_addr
    ]]

    mac_addr = ans:fetch()
    local mac_list = {}
    while mac_addr do
        table.insert(mac_list, mac_addr)
        mac_addr = ans:fetch()
    end
    ndata['mac_list'] = mac_list
    ans:close()

    ans = db:execute[[
    SELECT date(created_at) FROM DR_TEMPHUMI GROUP BY date(created_at)
    ]]

    created_at = ans:fetch()

    local render_date = created_at

    local date_list = {}
    while created_at do
        table.insert(date_list, created_at)
        created_at = ans:fetch()
    end
    ndata['date_list'] = date_list
    ans:close()

    luci.http.write(json.encode(ndata))
end

