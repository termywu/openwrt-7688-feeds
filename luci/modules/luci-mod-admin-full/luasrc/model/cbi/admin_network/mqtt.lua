local luci = {}
luci.sys = require "luci.sys"

local m, s, o

m = Map("mqtt", "MQTT Borker Setting", "Here you can configure the MQTT broker")
m:chain("luci")

s = m:section(TypedSection, "mqtt", "")
s.addremove = false
s.anonymous = true

h = s:option(Value, "hostname", "Broker Host")
h.datatype = "host"

o = s:option(Value, "topic", "Topic")

o = s:option(Value, "subscribe", "Subscribe Topic")

o = s:option(Value, "username", "User Name")

o = s:option(Value, "password", "Password")
o.password = true

o = s:option(Value, "update_interval", "update interval time")

function m.on_commit(map)
    luci.sys.exec("kill " .. map.uci:get("mqtt", "mqtt", "pid"))
    luci.sys.call("/usr/sbin/coapmanager.py &")
end


return m
